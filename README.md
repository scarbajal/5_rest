## Instructions

1. [forker le repo](https://bitbucket.org/log4420/5_rest/fork)
1. ```cd /home/tmp``` plus rapide à l'école
1. ```git clone git@bitbucket.org:______________/5_rest.git```
1. ```cd 5_rest```
1. dans un autre onglet: ```./mongo.sh``` téléchange et lance mongo [MONGO_D]
1. dans un autre onglet: ```/tmp/mongodb-linux-x86_64-2.2.1/bin/mongo``` pour lancer un sheel mongo [MONGO_SHELL]
1. copier la [license de jrebel](https://moodle.polymtl.ca/pluginfile.php/201395/mod_resource/content/1/jrebel.lic)
1. ```./sbt-jrebel.sh```
1. ```gen-idea```
1. ```~container:start```
1. dans [MONGO_SHELL] ```show dbs```
1. on doit voir ```log4420-rest```
1. ```use log4420-rest```
1. ```show collections```
1. on doit voir ```cheminements```
1. ```db.cheminements.find()``` affiche les cheminements
1. ```db.cheminements.remove()``` suprime les cheminements
1. des test se trouvent dans ```src/test/curl/test.sh```
1. on peut exécuter ```curl localhost:8080/cheminement``` pour voir tous les cheminements


### Example 

#### MongoDB Shell

```>db.cheminements.find()```
```
{ "_id" : ObjectId("50ad523fef863fdefdd81d08"), "owner" : "Génie Logiciel", "sessions" : [ 	"class ca.polymtl.log4420.model.Session={perriode=Perriode(Automne,2009), listeDeCours=List(Cours(INF1005C), Cours(INF1500), Cours(MTH1101), Cours(MTH1006), Cours(LOG3005I), Cours(INF1040))}", 	"class ca.polymtl.log4420.model.Session={perriode=Perriode(Hiver,2010), listeDeCours=List(Cours(INF1010), Cours(LOG1000), Cours(INF1600), Cours(MTH1102), Cours(INF1995))}" ], "titre" : "Classique" }
```

#### Test Curl

##### GET /cheminement

```curl localhost:8080/cheminement```

```
[{
  "owner":"Génie Logiciel",
  "sessions":["class ca.polymtl.log4420.model.Session={perriode=Perriode(Automne,2009), listeDeCours=List(Cours(INF1005C), Cours(INF1500), Cours(MTH1101), Cours(MTH1006), Cours(LOG3005I), Cours(INF1040))}","class ca.polymtl.log4420.model.Session={perriode=Perriode(Hiver,2010), listeDeCours=List(Cours(INF1010), Cours(LOG1000), Cours(INF1600), Cours(MTH1102), Cours(INF1995))}"],
  "_id":"50ad9d5eef86379d67fce332",
  "titre":"Classique"
},{
  "owner":"Génie Informatique",
  "sessions":["class ca.polymtl.log4420.model.Session={perriode=Perriode(Automne,2009), listeDeCours=List(Cours(INF1005C), Cours(INF1500), Cours(MTH1101), Cours(MTH1006), Cours(LOG3005I), Cours(INF1040))}","class ca.polymtl.log4420.model.Session={perriode=Perriode(Hiver,2010), listeDeCours=List(Cours(INF1010), Cours(LOG1000), Cours(INF1600), Cours(MTH1102), Cours(INF1995))}"],
  "_id":"50ad9d5eef86379d67fce333",
  "titre":"Multimedia"
}]
```

##### GET /cheminement/ObjectId

```curl localhost:8080/cheminement/50ad9d5eef86379d67fce332```
```
{
  "owner":"Génie Logiciel",
  "sessions":["class ca.polymtl.log4420.model.Session={perriode=Perriode(Automne,2009), listeDeCours=List(Cours(INF1005C), Cours(INF1500), Cours(MTH1101), Cours(MTH1006), Cours(LOG3005I), Cours(INF1040))}","class ca.polymtl.log4420.model.Session={perriode=Perriode(Hiver,2010), listeDeCours=List(Cours(INF1010), Cours(LOG1000), Cours(INF1600), Cours(MTH1102), Cours(INF1995))}"],
  "_id":"50ad9d5eef86379d67fce332",
  "titre":"Classique"
}
```

### Bareme

RestCheminement sur 100%
RestSession 1 & 2 bonus 20%

En équipe de deux

### Example Rest complet

[https://bitbucket.org/log4420/lift_rest](https://bitbucket.org/log4420/lift_rest)